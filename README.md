# Influx db study

## InfluxDB

InfluxDB is a time series and spatial database engine. Currently (March 2024),
is the most popular time series db engine according to
[db-engines.com](https://db-engines.com/en/ranking/time+series+dbms).

InfluxDB typical application scenarios are:

- IoT & Sensor Monitoring
- DevOps Monitoring & Tools for Developers
- Real-Time Analytics

Find more information about InfluxDB at.
[this link](https://db-engines.com/en/system/InfluxDB).

Follow [this guide](https://docs.influxdata.com/influxdb/v2/) to get started with
InfluxDB v2.

## Run the code

The code is containerized. Creating an InfluxDB database is easy as:

```bash
docker compose up
```

The [main.py](./src/main.py) script adds a simple point to the database and reads the values
inserted with the python InfluxDB client package.
